<?php
/**
 * Created by PhpStorm.
 * User: Vladimir Zabara <itsupport@rompos.com>
 * Date: 26.11.2019
 * Time: 13:27
 */
define('BASEPATH', __DIR__);

$shortopts = "hv";

$longopts = [
    "date::",
    "verbose",
    "dry-run",
    "help",
];

$options = getopt($shortopts, $longopts);

$verbose = isset($options['verbose']) || isset($options['v']);
$help = isset($options['help']) || isset($options['h']);
$dry_run = isset($options['dry-run']);

if ($help) {
    die('Usage: php export-report-data.php --date=2019-11-26 -v' . PHP_EOL .
        'Or   : php export-report-data.php ' . PHP_EOL);
}

if (!empty($options['date'])) {
    $date = date('Y-m-d', strtotime($options['date']));
} else {
    $date = date('Y-m-d', strtotime('yesterday'));
}

$date_before = date('Y-m-d', strtotime($date . ' +1 day'));
$date_after = date('Y-m-d', strtotime($date));

include_once __DIR__ . '/../application/modules/rompos/vendor/autoload.php';
include_once __DIR__ . '/../application/helpers/rompos_helper.php';
include_once __DIR__ . '/../config/database.php';
include_once __DIR__ . '/../config/reports.php';

$url = REPORTS_API_URL;
$oauth_token = REPORTS_API_TOKEN;

$curl = new Curl\Curl();

$pdo = new PDO("mysql:host={$db['default']['hostname']};dbname={$db['default']['database']};charset={$db['default']['char_set']}",
    $db['default']['username'], $db['default']['password']);
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
$dbPrefix = $db['default']['dbprefix']; //

$stmt = $pdo->prepare("SELECT `key` FROM {$dbPrefix}restapi_keys LIMIT 1");
$stmt->execute();
$apiKey = $stmt->fetchColumn() ?? '';
// check multi store mode
$stmt = $pdo->prepare("SELECT `value` FROM {$dbPrefix}options WHERE `key`=? LIMIT 1");
$stmt->execute([
    'nexo_store',
]);
$store_mode = $stmt->fetchColumn() ?? '';
$stores = [];
if (empty($store_mode) || $store_mode == 'disabled') {
    $stores = [0];
} else {
    $stmt = $pdo->prepare("SELECT * FROM {$dbPrefix}nexo_stores WHERE `status`=?");
    $stmt->execute([
        'opened',
    ]);
    $recs = $stmt->fetchAll();
    foreach ($recs as $store) {
        $stores[] = $store->ID;
    }
}

foreach ($stores as $store_id) {
    $prefix = $store_id ? 'store_' . $store_id . '_' : '';

    $client_id = 0;
    $stmt = $pdo->prepare("SELECT `value` FROM {$dbPrefix}options WHERE `key`='{$prefix}orders_client_id' LIMIT 1");
    $stmt->execute();
    $client_id = $stmt->fetchColumn() ?? '';
    if (empty($client_id)) {
        die("It seems client ID is wrong." . PHP_EOL);
    }

    // get client reports data
    $curl->setHeader('Accept', 'application/json');
    $curl->setHeader('Authorization', sprintf("Bearer %s", $oauth_token));
    $res = $curl->get($url . '/api/v1/client/' . $client_id);
    if ($curl->error) {
        if ($verbose) {
            print_r(['Curl error', $curl->errorMessage, $url, $curl->requestHeaders]);
        }
        die('Error: ' . $curl->errorMessage . PHP_EOL);
    }

    $commission = $res->data->commission ?? 0;

    if (!empty($url) || $dry_run) {
        // collect orders
        $stmt = $pdo->prepare("SELECT * FROM {$dbPrefix}{$prefix}nexo_commandes WHERE `DATE_CREATION` BETWEEN ? AND ?");
        $stmt->execute([
            $date_after,
            $date_before,
        ]);
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $orders = [];
        foreach ($rows as $row) {
            $order = [
                'code' => $row['CODE'],
                'type' => $row['RESTAURANT_ORDER_TYPE'],
                'subtype' => '',
                'total' => $row['SOMME_PERCU'],
                'fee' => 0,
                'client_id' => $client_id,
                'source_id' => 1, // In Place by default
                'status' => $row['TYPE'] == 'nexo_order_refunded' ? 'refunded' : $row['STATUS'],
                'created' => $row['DATE_CREATION'],
            ];

            // parse meta data
            $stmt = $pdo->prepare("SELECT * FROM {$dbPrefix}{$prefix}nexo_commandes_meta WHERE `REF_ORDER_ID`=?");
            $stmt->execute([
                $row['ID'],
            ]);
            $points = [
                'points_earned' => 0,
                'points_redeemed' => 0,
                'redemption' => 0.0,
            ];
            $meta_rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
            foreach ($meta_rows as $meta_row) {
                switch ($meta_row['KEY']) {
                    case '_wccf_cf_value_convenience':
                        // convenience fee
                        $order['fee'] = floatval($meta_row['VALUE']);
                        break;
                    case 'group_order':
                        // group order flag
                        $order['subtype'] = 'group';
                        break;
                    case '_ywpar_points_earned':
                        // YITH earned points
                        $points['points_earned'] = $meta_row['VALUE'];
                        break;
                    case '_ywpar_redemped_points':
                        // YITH redeemed points
                        $points['points_redeemed'] = $meta_row['VALUE'];
                        break;
                    case '_ywpar_coupon_amount':
                        // YITH redeemed amount
                        $points['redemption'] = $meta_row['VALUE'];
                        break;
                    case 'woocommerce_order_id':
                        // Online store
                        $order['source_id'] = 2;
                        break;
                    case 'seamless_order_id':
                        // Seamless order
                        $order['source_id'] = 4;
                        break;
                }
            }
            $order['points'] = $points;

            // collect customer info
            $stmt = $pdo->prepare("SELECT * FROM {$dbPrefix}{$prefix}nexo_clients WHERE `ID`=?");
            $stmt->execute([
                $row['REF_CLIENT'],
            ]);
            $customer = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($customer) {
                $order['customer'] = [
                    'name' => trim($customer['NOM'] . ' ' . $customer['PRENOM']),
                    'email' => $customer['EMAIL'],
                    'phone' => $customer['TEL'],
                    'created' => $customer['DATE_CREATION'],
                ];
            } else {
                $order['customer'] = [
                    'name' => 'Guest',
                    'email' => 'guest-' . microtime_float() . '@rompos.com',
                    'phone' => '',
                    'created' => date('Y-m-d H:i:s'),
                ];
            }
            // collect products
            $stmt = $pdo->prepare("SELECT * FROM {$dbPrefix}{$prefix}nexo_commandes_produits WHERE `REF_COMMAND_CODE`=?");
            $stmt->execute([
                $row['CODE'],
            ]);
            $rows2 = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $products = [];
            $productsCosts = 0;
            foreach ($rows2 as $product) {
                $prod = [
                    'title' => $product['NAME'],
                    'sku' => $product['REF_PRODUCT_CODEBAR'],
                    'quantity' => $product['QUANTITE'],
                    'price' => $product['PRIX'],
                    'sale_price' => $product['PRIX_BRUT'],
                    'total' => $product['PRIX_TOTAL'],
                ];

                $productsCosts += floatval($product['PRIX_TOTAL']);

                // collect products meta
                $stmt = $pdo->prepare("SELECT `VALUE` FROM {$dbPrefix}{$prefix}nexo_commandes_produits_meta WHERE `REF_COMMAND_PRODUCT`=? AND `KEY`=?");
                $stmt->execute([
                    $product['ID'],
                    'modifiers',
                ]);
                $meta = [];
                $productMeta = $stmt->fetchColumn() ?? '';
                if (!empty($productMeta)) {
                    $modifiers = json_decode($productMeta);
                    if (!empty($modifiers)) {
                        foreach ($modifiers as $modifier) {
                            $meta[] = [
                                'name' => $modifier->name,
                                'price' => $modifier->price ?? 0,
                            ];
                        }
                    }
                }
                $prod['modifiers'] = $meta;
                $products[] = $prod;
            }
            $order['products'] = $products;

            // calculate commission for online order (see API Reports "sources" DB table)
            // woocommerce - 2
            // mobile app - 5
            if (in_array($order['source_id'], [2, 5]) && $commission > 0) {
                $order['fee'] += floatval($productsCosts * $commission / 100);
            }

            // collect payments
            $stmt = $pdo->prepare("SELECT * FROM {$dbPrefix}{$prefix}nexo_commandes_paiements WHERE `REF_COMMAND_CODE`=?");
            $stmt->execute([
                $row['CODE'],
            ]);
            $rows4 = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $payments = [];
            foreach ($rows4 as $payment) {
                $payments[] = [
                    'amount' => $payment['MONTANT'],
                    'type' => $payment['PAYMENT_TYPE'],
                    'operation' => $payment['OPERATION'],
                    'created' => $payment['DATE_CREATION'],
                ];
            }
            $order['payments'] = $payments;

            $orders[] = $order;

        }

        if ($verbose) {
            print_r(['Store ID', $store_id]);
            print_r(['Prepared to send', $orders]);
        }

        $serialized = serialize($orders);

        if (!$dry_run) {
            $curl->setHeader('Accept', 'application/json');
            $curl->setHeader('Authorization', sprintf("Bearer %s", $oauth_token));
            $res = $curl->post($url . '/api/v1/orders/import', [
                'data' => $serialized,
            ]);
            if ($curl->error) {
                if ($verbose) {
                    print_r(['Curl error', $curl->error, $url, $curl->requestHeaders]);
                }
                die('Error: ' . $curl->error . PHP_EOL);
            }
        }
    } else {
        die('Daily Reports Sync URL is empty');
    }
}

function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}