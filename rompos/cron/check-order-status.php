<?php
/**
 * Created by PhpStorm.
 * User: Vladimir Zabara <itsupport@rompos.com>
 * Date: 04.03.2019
 * Time: 13:55
 */
define('BASEPATH', __DIR__);

define('TAKEAWAY_TIMER', 10);
define('DELIVERY_TIMER', 30);

include_once __DIR__ . '/../application/modules/rompos/vendor/autoload.php';
include_once __DIR__ . '/../config/database.php';
include_once __DIR__ . '/../application/helpers/rompos_helper.php';

$pdo = new PDO("mysql:host={$db['default']['hostname']};dbname={$db['default']['database']};charset={$db['default']['char_set']}", $db['default']['username'], $db['default']['password']);
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);

$dbPrefix = $db['default']['dbprefix']; //

$stmt = $pdo->prepare("SELECT `key` FROM {$dbPrefix}restapi_keys LIMIT 1");
$stmt->execute();
$apiKey = $stmt->fetchColumn() ?? '';
$curl = new Curl\Curl();
$curl->setHeader('X-API-KEY', $apiKey);

// get system timezone offset
$dt = new DateTime(null, new DateTimeZone(date('T')));
$offset1 = $dt->getOffset() / 3600;
// get MySQL timezone offset
$stmt = $pdo->query('SELECT @@system_time_zone');
$tz = $stmt->fetchColumn();
$dt = new DateTime(null, new DateTimeZone($tz));
$offset2 = $dt->getOffset() / 3600;
// offset in minutes
$offset = abs($offset1 - $offset2) * 60;

// check multi store mode
$stmt = $pdo->prepare("SELECT `value` FROM {$dbPrefix}options WHERE `key`=? LIMIT 1");
$stmt->execute([
    'nexo_store',
]);
$store_mode = $stmt->fetchColumn() ?? '';
$stores = [];
if (empty($store_mode) || $store_mode == 'disabled') {
    $stores = [0];
} else {
    $stmt = $pdo->prepare("SELECT * FROM {$dbPrefix}nexo_stores WHERE `status`=?");
    $stmt->execute([
        'opened',
    ]);
    $recs = $stmt->fetchAll();
    foreach ($recs as $store) {
        $stores[] = $store->ID;
    }
}
foreach ($stores as $store_id) {
    $prefix = $store_id ? 'store_' . $store_id . '_' : '';
    // get takeaway timer
    $stmt = $pdo->prepare("SELECT `value` FROM {$dbPrefix}options WHERE `key`='{$prefix}takeaway_timer' LIMIT 1");
    $stmt->execute();
    $takeawayTimer = $stmt->fetchColumn() ?? TAKEAWAY_TIMER;
    // get delivery timer
    $stmt = $pdo->prepare("SELECT `value` FROM {$dbPrefix}options WHERE `key`='{$prefix}delivery_timer' LIMIT 1");
    $stmt->execute();
    $deliveryTimer = $stmt->fetchColumn() ?? DELIVERY_TIMER;

    try {
        $params = [
            'pending',
            'booking',
            'takeaway',
            'dinein',
        ];
        $stmt = $pdo->prepare("UPDATE {$dbPrefix}{$prefix}nexo_commandes SET `RESTAURANT_ORDER_STATUS`=?, `DATE_MOD`=NOW() WHERE RESTAURANT_ORDER_STATUS=? AND RESTAURANT_ORDER_TYPE IN (?,?) AND NOW() >= DATE_SUB( `RESTAURANT_BOOKED_FOR`, INTERVAL " . ($offset + intval($takeawayTimer)) . " MINUTE)");
        $stmt->execute($params);

        // check delivery hold orders
        $params = [
            'pending',
            'booking',
            'delivery',
        ];
        $stmt = $pdo->prepare("UPDATE {$dbPrefix}{$prefix}nexo_commandes SET `RESTAURANT_ORDER_STATUS`=?, `DATE_MOD`=NOW() WHERE RESTAURANT_ORDER_STATUS=? AND RESTAURANT_ORDER_TYPE=? AND NOW() >= DATE_SUB( `RESTAURANT_BOOKED_FOR`, INTERVAL " . ($offset + intval($deliveryTimer)) . " MINUTE)");
        $stmt->execute($params);
    } catch (PDOException $e) {
        echo $e->getMessage() . PHP_EOL;
    }
}
