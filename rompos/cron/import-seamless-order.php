#!/usr/bin/php
<?php
/**
 * Created by PhpStorm.
 * User: Vladimir Zabara <itsupport@rompos.com>
 * Date: 04.03.2020
 * Time: 13:55
 */
define('BASEPATH', __DIR__);

$shortopts = "h";

$longopts = [];

$options = getopt($shortopts, $longopts);

if (isset($options['h'])) {
    die('Usage: php import-seamless-order.php' . PHP_EOL);
}

include_once __DIR__ . '/../application/modules/rompos/vendor/autoload.php';
include_once __DIR__ . '/../application/libraries/Seamless_Parser.php';
include_once __DIR__ . '/../config/database.php';
include_once __DIR__ . '/../config/seamless.php';
include_once __DIR__ . '/../application/helpers/rompos_helper.php';

$content = file_get_contents('php://stdin');
$html = '';
if (preg_match('~boundary="(.*[^"])"~', $content, $mm)) {
    $parts = explode($mm[1], $content);
    foreach($parts as $part) {
        if (preg_match('~<html>(.*)</html>~s', $part, $mm)) {
            $html = $mm[0];
            break;
        }
    }
}

if (empty($html)) {
    exit;
}

// get from file to test
// $html  = file_get_contents('email.html');

$order = (new Seamless_Parser($html))->parse();
if (empty($order->items) || empty($order->payments)) {
    send_email('Order wrong format', $order);
    die('Order wrong format');
}
$store_id = null;
foreach (SEAMLESS_STORES_IDS as $id=>$store) {
    if ($store == $order->info->store) {
        $store_id = $id;
        break;
    }
}
if (empty($store_id)) {
    send_email('Unknown store ID', $order);
    die('Unknown store ID');
}

$pdo = new PDO("mysql:host={$db['default']['hostname']};dbname={$db['default']['database']};charset={$db['default']['char_set']}",
    $db['default']['username'], $db['default']['password']);
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);

$dbPrefix = $db['default']['dbprefix']; //

$stmt = $pdo->prepare("SELECT `key` FROM {$dbPrefix}restapi_keys LIMIT 1");
$stmt->execute();
$apiKey = $stmt->fetchColumn() ?? '';
$curl   = new Curl\Curl();
$curl->setHeader('X-API-KEY', $apiKey);

$url       = SEAMLESS_IMPORT_URL . "/api/rompos/seamless_order/?store_id=" . $store_id;
$res       = $curl->post($url, [
    'data' => $order
]);

function send_email($subject, $data) {
    // print order
    ob_start();
    print_r($data);
    $v = ob_get_contents();
    ob_end_clean();
    //
    mail(SEAMLESS_REPORT_EMAIL, $subject, $v);
}
