<?php

define('RABBITMQ_HOST', 'localhost');
define('RABBITMQ_VHOST', '/');
define('RABBITMQ_PORT', 5672);
define('RABBITMQ_USER', 'guest');
define('RABBITMQ_PASSWORD', 'guest');
define('RABBITMQ_EXCHANGE', 'rompos');
define('RABBITMQ_QUEUE', 'test_orders');
