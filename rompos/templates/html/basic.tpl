<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<meta charset="utf-8">
<head>
    <link rel="stylesheet" href="style.css" />
</head>
<body>
<center>
    {% if print_prices %}
    <img src="logo.png" />
    {% endif %}
    <table>
        <tr><td>{{order_dt}}</td><td>{{order_tm}}</td></tr>
    </table>
    <hr/>
    {% if title is not empty %}
    <h1>{{title}}</h1>
    {% endif %}
    <h2>{{template.title}}</h2>
    <h2>{{template.customer_name}}</h2>
    {% if template.customer_phone is not empty %}
    <h3>Phone: {{template.customer_phone}}</h3>
    {% endif %}
    {% if order.email is not empty %}
    {#<h3>Email: {{order.email}}</h3>#}
    {% endif %}
    {% if order.RESTAURANT_ORDER_TYPE == 'delivery' %}
    {% if template.delivery_address_1 is not empty %}<h3>{{template.delivery_address_1}}</h3>{% endif %}
    {% if template.delivery_address_2 is not empty %}<h3>{{template.delivery_address_2}}</h3>{% endif %}
    {% endif %}
    {% if date is not empty %}
    <h2>{{date}}</h2>
    {% endif %}
    {% if time is not empty %}
    <h2>{{time}}</h2>
    {% endif %}
</center>
<hr/>
{% for item in items %}
<table class="items">
    {% if item.metas.customer is not null %}
    <tr><td colspan="2"><p>To: {{item.metas.customer}}</p></td></tr>
    {% endif %}
    <tr><td><h4>{{item.full.title}} (x{{item.full.quantity}})</h4>
        {% if item.modifiers is not null %}
        <ul>
        {% for modifier in item.modifiers %}
        <li>{{modifier}}</li>
        {% endfor %}
        </ul>
        {% endif %}
        {% if item.metas.restaurant_note is not empty %}
        <p>Notes: {{item.metas.restaurant_note}}</p>
        {% endif %}
    </td><td>{{item.full.total}}</td>
    </tr>
</table>
<hr/>
{% endfor %}
{% if order.metas.customer_note is not empty %}
<p>{{order.metas.customer_note}}</p>
<hr/>
{% endif %}
{% if print_prices %}
<table class="payments">
    <tr><td>Sub Total</td><td>{{subtotal_val}}</td></tr>
    {% if taxes is not empty %}<tr><td>Tax</td><td>{{taxes_val}}</td></tr>{% endif %}
    <tr><td>Discount</td><td>{{deduction_val}}</td></tr>
    {% if delivery is not empty %} <tr><td>Delivery Fee</td><td>{{delivery_val}}</td></tr>{% endif %}
    {% if fees is not empty %}
    {% for fee in allfees %}
    <tr><td>{{fee.label}}</td><td>{{fee.val}}</td></tr>
    {% endfor %}
    {% endif %}
    <tr><td>Total</td><td>{{total_val}}</td></tr>
    {% if refund is not empty %}
    <tr><td>Refund</td><td>{{refund_val}}</td></tr>
    {% endif %}
    <tr><td>Collected</td><td>{{collected_val}}</td></tr>
    <tr><td>{{due_label}}</td><td>{{due_val}}</td></tr>
</table>
<center>
<p>{{bills_notices}}</p>
{% if other_details is not empty %}
<hr/><p>{{other_details}}</p>
{% endif %}
</center>
{% endif %}
</body>
</html>
