<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html>
<head>

</head>
<body>
<div class="container" style="padding:20px;width:80%;margin:0 auto;">
    <h1>{{subject}}</h1>
    <h2>{{transfert.DATE_CREATION}}</h2>
    <p>More details at <a href="{{to_stock_history_url}}">Transfert History Page</a>.</p>
    <hr>
    <table style="width:90%;">
        <tr>
            <td style="padding:0 .7rem;">
                <p><strong><u>From</u>:</strong></p>
                <p>Store: <strong>{{provider_store_name}}</strong><br/>
                    Email: <strong>{{provider_store_email}}</strong><br/>
                    Phone: <strong>{{provider_store_phone}}</strong><br/>
                    Fax: <strong>{{provider_store_fax}}</strong><br/>
                    Address: <strong>{{provider_store_address}}</strong><br/>
                    P.O.Box: <strong>{{provider_store_pobox}}</strong></p>
            </td>
            <td style="padding:0 .7rem;">
                <p><strong><u>To</u>:</strong></p>
                <p>Store: <strong>{{receiver_store_name}}</strong><br/>
                    Email: <strong>{{receiver_store_email}}</strong><br/>
                    Phone: <strong>{{receiver_store_phone}}</strong><br/>
                    Fax: <strong>{{receiver_store_fax}}</strong><br/>
                    Address: <strong>{{receiver_store_address}}</strong><br/>
                    P.O.Box: <strong>{{receiver_store_pobox}}</strong></p>
            </td>
        </tr>
    </table>
    <hr>
    <table class="items" style="width:90%;">
        <tr>
            <th style="padding:0 .7rem;text-align:left;">Item Name</th>
            <th style="padding:0 .7rem;text-align:left;">Price</th>
            <th style="padding:0 .7rem;text-align:left;">Quantity</th>
            <th style="padding:0 .7rem;text-align:left;">Total</th>
        </tr>
        {% for item in items %}

        <tr>
            <td style="padding:0 .7rem;text-align:left;">{{item.DESIGN}}</td>
            <td style="padding:0 .7rem;text-align:left;">{{item.UNIT_PRICE}}</td>
            <td style="padding:0 .7rem;text-align:left;">{{item.QUANTITY}}</td>
            <td style="padding:0 .7rem;text-align:left;">{{item.TOTAL_PRICE}}</td>
        </tr>
        {% endfor %}
    </table>
</div>
</body>
</html>
