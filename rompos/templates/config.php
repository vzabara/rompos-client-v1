<?php
/**
 * Note: vendor is a directory name.
 */
return [
    [
        'title'    => __( 'Universal', 'restaurant' ),
        'vendor' => 'html',
    ],
    [
        'title'    => __( 'Epson', 'restaurant' ),
        'vendor' => 'epson',
    ],
    [
        'title'    => __( 'Star Micronics', 'restaurant' ),
        'vendor' => 'star',
    ],
];
