<?php

/*
 *---------------------------------------------------------------
 * APPLICATION ENVIRONMENT
 *---------------------------------------------------------------
 *
 * You can load different configurations depending on your
 * current environment. Setting the environment also influences
 * things like logging and error reporting.
 *
 * This can be set to anything, but default usage is:
 *
 *     development
 *     testing
 *     production
 *
 * NOTE: If you change these, also change the error_reporting() code below
 */
define('ENVIRONMENT', 'development');

/*
 *---------------------------------------------------------------
 * ERROR REPORTING
 *---------------------------------------------------------------
 *
 * Different environments will require different levels of error reporting.
 * By default development will show errors but testing and live will hide them.
 */
switch (ENVIRONMENT) {
    case 'development':
        error_reporting(-1);
        ini_set('display_errors', 1);
    break;

    case 'testing':
    case 'production':

        // ini_set('display_errors', 1);

        if (version_compare(PHP_VERSION, '5.3', '>=')) {
            error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);
        } else {
            error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_USER_NOTICE);
        }
    break;

    default:
        header('HTTP/1.1 503 Service Unavailable.', true, 503);
        echo 'The application environment is not set correctly.';
        exit(1); // EXIT_ERROR
}

/*
 *----------------------
 * TIMEZONE
 *----------------------
 *
 * If timezone has not been set, give it a default
 *
 */
if (ini_get('date.timezone') == '' ) {
        date_default_timezone_set('GMT');
}


$root_path = realpath( __DIR__ . '/../rompos/' );


/*
 *---------------------------------------------------------------
 * SYSTEM FOLDER NAME
 *---------------------------------------------------------------
 *
 * This variable must contain the name of your "system" folder.
 * Include the path if the folder is not in the same directory
 * as this file.
 */
$system_path = realpath( $root_path . '/system/' );

/*
 *---------------------------------------------------------------
 * APPLICATION FOLDER NAME
 *---------------------------------------------------------------
 *
 * If you want this front controller to use a different "application"
 * folder than the default one you can set its name here. The folder
 * can also be renamed or relocated anywhere on your server. If
 * you do, use a full server path. For more info please see the user guide:
 * http://codeigniter.com/user_guide/general/managing_apps.html
 *
 * NO TRAILING SLASH!
 */
$application_folder = realpath( $root_path . '/application/' );
$config_folder = realpath( $root_path . '/config/' );
$view_folder = realpath( $application_folder . '/views/' );
$logs_folder = realpath( $root_path . '/logs/' );
$cache_folder = realpath( $root_path . '/cache/' );
$templates_folder = realpath( $root_path . '/templates/' );

// The name of THIS file
define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));
// Path to the system folder
define('BASEPATH', $system_path . '/');
// Path to the front controller (this file)
define('FCPATH', __DIR__ . '/');
define('APPPATH', $application_folder . '/');
define('VIEWPATH', $view_folder . '/');
define('LOGSPATH', $logs_folder . '/');
define('CACHEPATH', $cache_folder . '/');
define('CONFIGPATH', $config_folder . '/');
define('TWIGPATH', $templates_folder . '/');

/*
 * --------------------------------------------------------------------
 * LOAD THE BOOTSTRAP FILE
 * --------------------------------------------------------------------
 *
 * And away we go...
 */
require_once BASEPATH . '/core/CodeIgniter.php';
